
    <div class="card mb-3">
        @if($project->status == 'En Proceso')
        <div class="text-white px-2 text-center bg-info">{{ $project->status }}</div>
        @endif

        @if($project->status == 'Terminado')
        <div class="text-white px-2 text-center bg-success">{{ $project->status }}</div>
        @endif

        @if($project->status == 'Atrasado')
        <div class="text-white px-2 text-center bg-warning">{{ $project->status }}</div>
        @endif

        @if($project->status == 'Cancelado')
        <div class="text-white px-2 text-center bg-danger">{{ $project->status }}</div>
        @endif

        <div class="card-body">
            <h5>{{ $project->name }}</h5>
   		    <p>{{ $project->description }}</p>
            @foreach($project->users as $user)
		        <p>{{ $user->name }}</p>
		    @endforeach
            <hr>
            <a href="" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#modalCrearTareas_{{ $project->id }}">Crear Tarea</a>
            <br>
            <br>
            @foreach($project->tasks as $task)
            <div>
                <div style="width:60%">
                    <p class="mb-0">{{ $task->title }}</p> 
                    
                    <p class="mb-0">A cargo: {{ $task->user }}</p>

                    @if($task->is_complete==false)
                    <span class="badge badge-warning">Pendientes</span>
                    @else
                    <span class="badge badge-warning">Completada</span>
                    @endif
                </div>
            </div>
                <div>
                @if($task->is_complete == false)
                    <a href="{{ route('tareas.status', $task->id) }}" class="btn btn-outline-success btn-sm"><ion-icon name="checkmark-outline"></ion-icon></a>
                    @endif
                    <a href="{{ route('tareas.edit', $task->id) }}" class="btn btn-outline-info btn-sm"><ion-icon name="pencil-outline"></ion-icon></a>
                    <form method="POST" action="{{ route('tareas.destroy', $task->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="submit" class="btn btn-danger btn-sm"><ion-icon name="trash-outline"></ion-icon></button>

                    </form>
                </div>

            @endforeach
            <hr>
            <p>Creado el: {{ Carbon\Carbon::parse($project->created_at)->diffForHumans() }}</p>
        </div> 
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalCrearTareas_{{ $project->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear Tarea</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action=" {{ route('tareas.store') }} ">
                {{csrf_field() }}

                <input type="hidden" name="source" value="proyectos" readonly="">
                <input type="hidden" name="project_id" value="{{ $project->id }}" readonly="">
                <input type="hidden" name="user_id" value="{{ $project->id }}" readonly="">
                
                <div class="modal-body">
                    <div class="form-grpup">
                            <label>Titulo Tareas</label>
                            <input type="text" name="title" class="form-control" required="">
                        </div>

                        <div class="form-grpup">
                            <label>Fecha de Entrega</label>
                            <input type="date" name="deadline" class="form-control">
                        </div>

                        <div class="form-grpup">
                            <label>Descripcion</label>
                            <input name="description" class="form-control" row="5">
                        </div>
                </div>      
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Guardar Tarea</button>
                </div>
            </form>
            </div>
        </div>
    </div>
        
