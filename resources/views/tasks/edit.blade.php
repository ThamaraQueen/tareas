@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">CREAR TAREA</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('tareas.update', $task->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-grpup">
                            <label>Titulo Tareas</label>
                            <input type="text" name="title" class="form-control" value="{{ $task->title }}" required="">
                        </div>

                        <div class="form-grpup">
                            <label>Fecha de Entrega</label>
                            <input type="date" name="deadline" value="{{ $task->deadline }}" class="form-control">
                        </div>

                        <div class="form-grpup">
                            <label>Descripcion</label>
                            <textarea name="description" class="form-control" row="5">{{ $task->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="SelectUserId">Selecciona usuario</label>
                            <select class="form-control" id="SelectUserId" name="user_id" multiple="">
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-dark"> Guardar </button>
                        
                        <a href="{{ route('tareas.index') }}" class="btn btn-outline-dark">Cancelar</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection