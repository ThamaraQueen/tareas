@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">CREAR TAREA</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('tareas.store') }}">
                        {{ csrf_field() }}
                        <div class="form-grpup">
                            <label>Titulo Tareas</label>
                            <input type="text" name="title" class="form-control" required="">
                        </div>

                        <div class="form-grpup">
                            <label>Fecha de Entrega</label>
                            <input type="date" name="deadline" class="form-control">
                        </div>

                        <div class="form-grpup">
                            <label>Descripcion</label>
                            <input name="description" class="form-control" row="5">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Selecciona usuario</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="user_id">
                                @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
				        </div>

                        <br>
                        <button type="submit" class="btn btn-dark"> Guardar </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection