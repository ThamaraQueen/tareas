@if(Session::get('edit'))
<div class="alert alert-primary alert-dismissible fade show" role="alert">
  <strong>Alerta</strong> {{ Session::get('edit') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if(Session::get('borrar'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Alerta</strong> {{ Session::get('borrar') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if(Session::get('exito'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Alerta</strong> {{ Session::get('exito') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if(Session::get('info'))
<div class="alert alert-primary alert-dismissible fade show" role="alert">
  <strong>Alerta</strong> {{ Session::get('info') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif